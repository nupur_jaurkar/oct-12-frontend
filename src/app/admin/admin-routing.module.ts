import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { RegUserBlogsComponent } from './components/reg-user-blogs/reg-user-blogs.component';
import { WriteBlogsComponent } from './components/reg-user-blogs/components/write-blogs/write-blogs.component';
import { BlogListingComponent } from './components/reg-user-blogs/components/blog-listing/blog-listing.component';
import { EditBlogsComponent } from './components/reg-user-blogs/components/edit-blogs/edit-blogs.component';
import { ManageUsersComponent } from '../admin/components/manage-users/manage-users.component';
import { AddUserComponent } from './components/manage-users/components/add-user/add-user.component';
import { EditUsersComponent } from './components/manage-users/components/edit-users/edit-users.component';
import { ManageProductsComponent } from './components/manage-products/manage-products.component';
import { EditProductsComponent } from './components/manage-products/components/edit-products/edit-products.component';
import { AddProductsComponent } from './components/manage-products/components/add-products/add-products.component';


const routes: Routes = [
  {
    path:'',
    component:DashboardComponent
  },
  {
    path:'contact-us',
    component:ContactUsComponent
  },
  {
    path:'reg-user-blogs',
    component:RegUserBlogsComponent
  },
  {
    path:'write-blogs',
    component:WriteBlogsComponent
  },
  {
   path:'blogs-listing',
   component:BlogListingComponent
  },
  {
    path:'edit-blogs/:id',
    component:EditBlogsComponent
  },
  {
    path:'manage-users',
    component:ManageUsersComponent
  },
  {
    path:'add-users',
    component:AddUserComponent
  },
  {
    path:'edit-users/:id',
    component:EditUsersComponent
  },
  {
    path:'manage-products',
    component:ManageProductsComponent
  },
  {
    path:'edit-products/:id',
    component: EditProductsComponent
  },
  {
    path:'add-products',
    component:AddProductsComponent
  },
];

@NgModule({
  imports: [   
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
