import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { RegUserBlogsComponent } from './components/reg-user-blogs/reg-user-blogs.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { WriteBlogsComponent } from './components/reg-user-blogs/components/write-blogs/write-blogs.component';
import { BlogListingComponent } from './components/reg-user-blogs/components/blog-listing/blog-listing.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { EditBlogsComponent } from './components/reg-user-blogs/components/edit-blogs/edit-blogs.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AdminRoutingModule } from './admin-routing.module';
import { ManageUsersComponent } from './components/manage-users/manage-users.component';
import { AddUserComponent } from './components/manage-users/components/add-user/add-user.component';
import { EditUsersComponent } from './components/manage-users/components/edit-users/edit-users.component';
import { ManageProductsComponent } from './components/manage-products/manage-products.component';
import { AddProductsComponent } from './components/manage-products/components/add-products/add-products.component';
import { EditProductsComponent } from './components/manage-products/components/edit-products/edit-products.component';
// import { FileUploader,FileSelectDirective } from 'ng2-file-upload';
import {EditorModule} from 'primeng/editor';
import {TableModule} from 'primeng/table';
import {ButtonModule} from 'primeng/button';
import {PaginatorModule} from 'primeng/paginator';
import {CardModule} from 'primeng/card';



@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    Ng2SearchPipeModule,
    EditorModule,
    TableModule,
    ButtonModule,
    PaginatorModule,
    CardModule,
   
    // FileUploader,
    // FileSelectDirective
  ],
  declarations: [DashboardComponent, 
    ContactUsComponent,
    RegUserBlogsComponent,
    NavbarComponent,
    FooterComponent, 
    WriteBlogsComponent, 
    BlogListingComponent, 
    EditBlogsComponent, 
    ManageUsersComponent, 
    AddUserComponent, 
    EditUsersComponent, 
    ManageProductsComponent, 
    AddProductsComponent, 
    EditProductsComponent,]
})
export class AdminModule { }
