import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.css']
})
export class ManageUsersComponent implements OnInit {
  userslist:any;

  constructor(private users:UserService,private router :Router) { }

  ngOnInit() {
    this.getDetails()  
  }

  getDetails(){
    this.users.listUsers().subscribe(res => {
      this.userslist = res;
      console.log("users",this.userslist);
    });
  }

  deleteUsers(id){
    this.users.deleteUsers(id).subscribe(res => {
      console.log('Deleted');    
    // this.router.navigate(['blogs-listing/blogs-listing'])
    location.reload()
    });
    // this.router.navigate(['blogs-listing/blogs-listing'])
  }
}
