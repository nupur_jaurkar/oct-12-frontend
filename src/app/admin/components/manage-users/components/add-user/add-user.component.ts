import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../../services/user.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  rForm: FormGroup;
  post:any;                     // A property for our submitted form
  password:string = '';
  username:string = ''; 
  email:string = '';  
  imagesBlog:string = '';
  titleAlert:string = 'This field is required';
  loading: boolean = false;
  role:string = '';
  file:null;

  constructor(private fb: FormBuilder,
    private service:UserService, private router: Router,) { 
      this.rForm = fb.group({
        'username' : [null, Validators.required],
        'password' : [null, Validators.required],
        'email': [null, Validators.required],
        'role':["admin", Validators.required],
        'validate' : '',
      });
      
    }

  ngOnInit() {
  }

addUsers(post:any) {
    this.service.addUsers(post).subscribe((res:any)=>{
      console.log("User added!::",res)
      if(res.code=200){
        alert(res.message)
      }
    })
}

}
