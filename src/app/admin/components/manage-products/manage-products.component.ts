import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-manage-products',
  templateUrl: './manage-products.component.html',
  styleUrls: ['./manage-products.component.css']
})
export class ManageProductsComponent implements OnInit {
  productslist:any;

  constructor(private products:UserService,private router :Router) { }

  ngOnInit() {
    this.getDetails() 
  }
  getDetails(){
    this.products.listProducts().subscribe(res => {
      this.productslist = res;
      console.log("products",this.productslist);
    });
  }
  deleteProducts(id){
    this.products.deleteProducts(id).subscribe(res => {
      console.log('Deleted');    
    // this.router.navigate(['blogs-listing/blogs-listing'])
    location.reload()
    });
    // this.router.navigate(['blogs-listing/blogs-listing'])
  }
}
