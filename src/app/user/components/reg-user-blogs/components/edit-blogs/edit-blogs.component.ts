import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { UserService } from '../../../../../services/user.service';
import { ReactiveFormsModule } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-blogs',
  templateUrl: './edit-blogs.component.html',
  styleUrls: ['./edit-blogs.component.css']
})
export class EditBlogsComponent implements OnInit {
  rForm: FormGroup;
  post:any;                     // A property for our submitted form
  description:string = '';
  name:string = '';   
  titleAlert:string = 'This field is required';

  constructor(private fb: FormBuilder,private route:ActivatedRoute,
    private service:UserService, private router: Router,) {
      this.rForm = fb.group({
        'name' : [null, Validators.required],
        'description' : [null, Validators.compose([Validators.required, Validators.minLength(30), Validators.maxLength(9000)])],
        'validate' : ''
      });
     }

  ngOnInit() {

     this.rForm.get('validate').valueChanges.subscribe(

      (validate) => {

          if (validate == '1') {
              this.rForm.get('name').setValidators([Validators.required, Validators.minLength(3)]);
              this.titleAlert = 'You need to specify at least 3 characters';
          } else {
              this.rForm.get('name').setValidators(Validators.required);
          }
          this.rForm.get('name').updateValueAndValidity();

      });
  }
editBlogs(data, id){
  this.route.params.subscribe(params =>{
    this.service.editBlogs(params.id,this.rForm.value).subscribe((res:any)=>{
      console.log("Edit your blog!::",res)
      if(res.code=200){
        alert(res)
      }
    })
  })
 
}
}
