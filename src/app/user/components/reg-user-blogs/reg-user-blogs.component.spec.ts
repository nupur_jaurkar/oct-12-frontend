import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegUserBlogsComponent } from './reg-user-blogs.component';

describe('RegUserBlogsComponent', () => {
  let component: RegUserBlogsComponent;
  let fixture: ComponentFixture<RegUserBlogsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegUserBlogsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegUserBlogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
