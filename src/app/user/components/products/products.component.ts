import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  productslist:any;

  constructor(private products:UserService,private router :Router) { }

  ngOnInit() {
    this.getDetails() 
  }

  getDetails(){
    this.products.listProducts().subscribe(res => {
      this.productslist = res;
      console.log("products",this.productslist);
    });
  }






  
}
